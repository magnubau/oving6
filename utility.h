#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char *replaceString(const char *str, size_t len) {
  char amp[5] = "&amp;";
  char lt[4] = "&lt;";
  char gt[4] = "&gt;";

  char *res;
  res = malloc(len * 5 + 1);
  // printf("%s\n", strlen(res));
  int k = 0;
  for (size_t i = 0; i < len;) {
    if (str[i] == '&') {
      for (int j = 0; j < 5; j++, k++) {
        res[k] = amp[j];
      }
      i++;
    } else if (str[i] == '<') {
      for (int j = 0; j < 4; j++, k++) {
        res[k] = lt[j];
      }
      i++;
    } else if (str[i] == '>') {
      for (int j = 0; j < 4; j++, k++) {
        res[k] = gt[j];
      }
      i++;
    } else {
      res[k] = str[i];
      k++;
      i++;
    }
  }
  res = realloc(res, k);
  return res;
}